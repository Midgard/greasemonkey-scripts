# Midgard’s Greasemonkey scripts

* [Linkify more tags on OpenStreetMap.org](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/linkify-more-tags-on-openstreetmap-org.user.js): add links to Panoramax and Mapillary, in the future maybe more
* [Lazyload images](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/lazyload-images.user.js): load lazy-loading images, without allowing the site's JavaScript
* [Deblur Wix images](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/deblur-wix-images.user.js) without allowing their JavaScript (add a *user include* if you come across a site with a proper domain name that uses Wix)
* [Prettier Rust book](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/prettier-rust-book.user.js)
* [Remove Quora signup wall](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/remove-quora-signup-wall.user.js)
* [imgur without their JavaScript](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/imgur-without-their-js.user.js)
* [Gentoo dropdowns without their JavaScript](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/gentoo-dropdowns-without-their-js.user.js)
* [Big map view](https://framagit.org/Midgard/greasemonkey-scripts/raw/master/factory-idle-big-map.user.js) for [Factory idle](https://factoryidle.com/)
