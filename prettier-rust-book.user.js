// ==UserScript==
// @name     Prettier Rust book
// @version  2
// @grant    none
// @include  https://doc.rust-lang.org/book/*
// @include  https://doc.rust-lang.org/unstable-book/*
// @include  file:///usr/share/doc/rust/html/book/*.html
// @include  file:///usr/share/doc/rust/html/unstable-book/*.html
// @include  https://doc.redox-os.org/book/*
// ==/UserScript==


let st = document.createElement("style");
st.innerHTML = `
html {
	scrollbar-color: #555 #111;
}

::selection {
	background-color: #456585;
	color: #fff;
}

@media (min-width: 1200px) {
	.content main {
		margin-left: 25%;
	}
}

body {
	font-family: CMU Serif;
	font-size: 150%;
	line-height: 1.75;
	text-rendering: optimizeLegibility;
}

body, nav, .light {
	color: #fff;
	background-color: #111;
}
.light .sidebar {
	color: #555;
	background-color: #111;
	scrollbar-color: #222 #111;
	scrollbar-width: thin;
	box-shadow: 0 0 0 rgba(1,1,1,0.5);
	transition: color 0.2s, background-color 0.2s, scrollbar-color 0.2s, box-shadow 0.2s;
}
.light .chapter li, .light .chapter li a {
	color: inherit;
}
.light .chapter li .active {
	color: #004c6f;
	transition: color 0.2s;
}
.light .sidebar:hover {
	color: #ddd;
	background-color: #181818;
	scrollbar-color: #555 #181818;
	box-shadow: 0 1em 1em rgba(0,0,0,0.5);
}
.light .sidebar:hover .chapter li .active {
	color: #008cff;
}

code {
	font-family: "Fira Code";
}

.light .content .header:link, .light .content .header:visited {
	color: #ddd;
}
.light #menu-bar > #menu-bar-sticky-container {
	background-color: #222;
	color: #aaa;
	box-shadow: 0 0 1em rgba(0,0,0,0.5);
	border-bottom: none;
}
#menu-bar {
	position: static;
}
.light blockquote {
	background-color: #2e2b29;
	color: #fff;
	box-shadow: 0 0 1em rgba(0,0,0,0.5);
}
body.light .does_not_compile, body.light .panics, body.light .not_desired_behavior,
body.rust .does_not_compile, body.rust .panics, body.rust .not_desired_behavior {
	background-color: #322;
}

pre {
	white-space: pre-wrap;
}
@media (min-width: 1600px) {
	pre {
		margin: 0 -250px 0 -100px;
	}
	pre.playpen {
		margin: 0;
	}
}
.hljs, .light :not(pre) > .hljs {
	background-color: #2e2b29;
	color: #e1efe1;
	box-shadow: 0 0 1em rgba(0,0,0,0.5);
}

.light .mobile-nav-chapters {
	background-color: #444;
}

.light .menu-bar, .light .menu-bar:visited, .light .nav-chapters, .light .nav-chapters:visited,
.light .mobile-nav-chapters, .light .mobile-nav-chapters:visited, .light .menu-bar .icon-button, .light .menu-bar a i {
	color: #333;
}
.light .menu-bar i:hover, .light .menu-bar .icon-button:hover, .light .nav-chapters:hover,
.light .mobile-nav-chapters i:hover {
	color: #ccc;
}

.fa-print {
	display: none;
}
.light pre > .buttons {
	color: #424149;
}
`;

document.body.appendChild(st);
