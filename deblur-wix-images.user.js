// ==UserScript==
// @name     Deblur Wix images
// @version  1
// @grant    none
// @include  https://*.wixsite.com/*
// ==/UserScript==

for (let img of document.getElementsByTagName("img")) {
	let m = img.src.match(/^(.*)\/v[0-9]*\/.*blur.*\.jpg$/);
	if (m !== null) {
		console.debug(`Fixing src from ${img.src} to ${m[1]}`);
		img.src = m[1];
	}
}
