// ==UserScript==
// @name     Factory idle big map
// @version  1
// @grant    none
// @include  https://factoryidle.com/*
// @include  http://factoryidle.com/*
// ==/UserScript==

let style = document.createElement("style");
style.innerHTML = `
#main {
	width: unset !important;
	margin: 10px;
}
#gameArea {
	width: unset;
	float: none;
}
.componentsArea {
	width: 210px;
}
.mapArea {
	position: relative;
}
.mapContainer {
	height: 1px;
}
.mapContainer > * {
	width: unset !important;
	height: calc(100vh - 270px) !important;
	position: absolute;
	left: 0;
	right: 0;
}
`;
document.body.appendChild(style);
