// ==UserScript==
// @name     imgur without their JS
// @version  1
// @grant    none
// @include  https://imgur.com/*
// @exclude  https://imgur.com/
// @exclude  https://imgur.com/about
// @exclude  https://imgur.com/blog
// @exclude  https://imgur.com/tos
// @exclude  https://imgur.com/privacy
// @exclude  https://imgur.com/apps
// @exclude  https://imgur.com/advertise
// @exclude  https://imgur.com/removalrequest
// @exclude  https://imgur.com/rules
// @exclude  https://imgur.com/upload
// @exclude  https://imgur.com/signin
// ==/UserScript==

for (let imageCont of document.getElementsByClassName("post-image-container")) {
	let id = imageCont.id;
	imageCont.getElementsByClassName("post-image")[0].outerHTML =
		"<a class='post-image' target='_blank' href='/" + id + ".jpg'><img src='/" + id + ".jpg' style='max-width:100%' /></a>";
}
