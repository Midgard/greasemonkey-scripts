// ==UserScript==
// @name     Remove Quora signup wall
// @version  1
// @grant    none
// @include https://www.quora.com/*
// ==/UserScript==

(function () {
	for (let div of document.getElementsByTagName("div")) {
		if (div.id.endsWith("signup_wall_wrapper")) {
			div.parentElement.removeChild(div);
		}
	}
})();
