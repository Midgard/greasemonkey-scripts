// ==UserScript==
// @name     Gentoo dropdowns without JS
// @version  1
// @grant    none
// @include  https://gentoo.org/*
// @include  https://*.gentoo.org/*
// @exclude  https://bugs.gentoo.org/*
// @exclude  https://sources.gentoo.org/*
// ==/UserScript==

(function() {
	let dropdown_style_codeblock = document.createElement("style");
	dropdown_style_codeblock.innerHTML = `
		.dropdown-toggle:focus + .dropdown-menu,
		.dropdown-toggle:hover + .dropdown-menu,
		.dropdown-menu:hover
		{
			display: block;
		}`;
	document.body.append(dropdown_style_codeblock);
})();
