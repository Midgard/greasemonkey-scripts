// ==UserScript==
// @name     Lazyload images
// @version  1
// @grant    none
// ==/UserScript==

function is_scam_placeholder(src_str) {
	return (
		src_str.length < 1 ||
		(src_str.startsWith("data:image/png") && src_str.length < 250) ||
		(src_str.startsWith("data:image/svg") && src_str.length < 150)
	);
}

for (let img of document.getElementsByTagName("img")) {
	if (img.dataset["src"] && ["lazyload", "lazyloading", "b-lazy", "lazy", "thumb-image"].some(needle => img.classList.contains(needle)) ) {
		img.src = img.dataset["src"];
		img.style = 'opacity: initial';
	}
	else if (img.dataset["lazyLoadSrc"] && is_scam_placeholder(img.src)) {
		img.src = img.dataset["lazyLoadSrc"];
	}
	else if (img.dataset["scoSrc"] && is_scam_placeholder(img.src)) {
		img.src = img.dataset["scoSrc"];
		img.style += ";opacity: initial;";
	}
}
