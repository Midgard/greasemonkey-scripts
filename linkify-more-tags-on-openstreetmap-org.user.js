// ==UserScript==
// @name     Linkify more tags on OpenStreetMap.org
// @version  2
// @grant    none
// @include  https://openstreetmap.org/*
// @include  https://www.openstreetmap.org/*
// ==/UserScript==

// Written by https://osm.org/user/M!dgard
//
// This program is free software: you can redistribute it and/or modify it under the terms of the
// GNU Affero General Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
// the GNU Affero General Public License for more details.
//
// For a copy of the GNU Affero General Public License, see <https://www.gnu.org/licenses/>.

"use strict";


/**
 * These definitions will be evaluated for each subvalue after splitting multiple values
 * (see valueSplitters).
 *
 * Don't forget to anchor your RegExps as required! (with ^ and $)
 *
 * The browser's developer console will have a log message when a key matched but a value didn't.
 *
 * matchKey (optional): RegExp to select keys
 * matchValue (optional): RegExp to filter subvalues to transform
 * condition (optional): extra condition that should be fulfilled in order to match:
 *     function from OSM key, value and tag-object to boolean
 * uri (required): function from OSM key and value to the URI that should be linked to
 * title (optional): function from OSM key and value to title of the hyperlink
 *     (the browser shows this when hovering over the hyperlink)
 */
const tagFormatters = [

	// Mapillary street view imagery
	{
		matchKey: /(^|:)mapillary(:[0-9]+)?$/,
		matchValue: /^[0-9]+$/,
		uri: (key, value) => "https://www.mapillary.com/app/?pKey=" + value,
		title: (key, value) => "Open this Mapillary image on mapillary.com (Mapillary is owned and " +
			"hosted by Facebook/Meta)",
	},

	// Panoramax street view imagery
	{
		matchKey: /(^|:)panoramax(:[0-9]+)?/,
		matchValue: /^[0-9a-z-]+$/,
		uri: (key, value) => "https://api.panoramax.xyz/#focus=pic&pic=" + value,
		title: (key, value) => "Open this Panoramax image on api.panoramax.xyz",
	},

	// Bus stops in Flanders
	{
		matchKey: /^ref(:De_Lijn)?$/,
		matchValue: /^[0-9]{6}$/,
		condition: (key, value, tags) => key == "ref:De_Lijn" || tags["operator"] === "De Lijn",
		uri: (key, value) => "https://www.delijn.be/nl/haltes/" + value + "/",
		title: (key, value) => "Search for this line number on the website of De Lijn",
	},
	{
		matchKey: /^route_ref(:De_Lijn)?$/,
		matchValue: /^[A-Z0-9]+$/,
		condition: (key, value, tags) => key == "route_ref:De_Lijn" || tags["operator"] === "De Lijn",
		uri: (key, value) => "https://www.delijn.be/nl/lijnen/?q=" + value,
		title: (key, value) => "Visit the website of De Lijn for stop " + value,
	},

	// turn:lanes wiki pages
	// Not very useful since these tags don't have wiki pages of their own. Just to demonstrate
	// that splitting on | in combination with valueSplitters (see below) works
	/*
	{
		matchKey: /turn(:lanes)?(:forward|backward|both_ways)?/,
		matchValue: /^(none|left|right|through|reverse|(merge_to|slight|sharp|slide|next)_(right|left))$/,
		uri: (key, value) => "https://wiki.openstreetmap.org/wiki/Tag:turn=" + value,
	},
	*/
];


/**
 * Definitions of how values should be split. Each value will be formatted independently.
 *
 * matchKey and matchValue as above
 * valueSeparator (optional): false to forbid splitting the value,
 *     or a RegExp to split multiple values, defaults to defaultValueSeparator.
 *     The RegExp must contain precisely one capturing group, around everything else.
 */
const defaultValueSeparator = /(\s*;\s*)/;
const valueSplitters = [
	{
		matchKey: /:lanes$/,
		valueSeparator: /(\s*[;|]\s*)/,
	},
];

const interval = 1.0;




/**
 * A utility function for userscripts that detects and handles AJAXed content.
 * From https://raw.githubusercontent.com/CoeJoder/waitForKeyElements.js/refs/heads/master/waitForKeyElements.js
 *
 * @example
 * waitForKeyElements("div.comments", (element) => {
 *   element.innerHTML = "This text inserted by waitForKeyElements().";
 * });
 *
 * waitForKeyElements(() => {
 *   const iframe = document.querySelector('iframe');
 *   if (iframe) {
 *     const iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
 *     return iframeDoc.querySelectorAll("div.comments");
 *   }
 *   return null;
 * }, callbackFunc);
 *
 * @param {(string|function)} selectorOrFunction - The selector string or function.
 * @param {function}          callback           - The callback function; takes a single DOM element as parameter.
 * @param {number}            [interval=300]     - The time (ms) to wait between iterations.
 */
function waitForKeyElements(selectorFunction, callback, interval) {
	if (typeof interval === "undefined") {
		interval = 300;
	}
	if (typeof waitForKeyElements.namespace === "undefined") {
		waitForKeyElements.namespace = Date.now().toString();
	}
	const targetNodes = selectorFunction();

	let targetsFound = targetNodes && targetNodes.length > 0;
	if (targetsFound) {
		targetNodes.forEach(targetNode => {
			const attrAlreadyFound = `data-userscript-${waitForKeyElements.namespace}-alreadyFound`;
			const alreadyFound = targetNode.getAttribute(attrAlreadyFound) || false;
			if (!alreadyFound) {
				callback(targetNode);
				targetNode.setAttribute(attrAlreadyFound, true);
			}
		});
	}

	setTimeout(
		() => waitForKeyElements(selectorFunction, callback, interval),
		interval
	);
}


function tagsOfTbody(tbody) {
	let result = {};
	for (let tr of tbody.querySelectorAll("tr")) {
		const [key, value] = keyValueOfTableRow(tr);
		result[key] = value;
	}
	console.log(result);
	return result;
}

function keyValueOfTableRow(tr) {
	return [
		tr.querySelector("th").innerText,
		tr.querySelector("td").innerText,
	];
}

function matchingEntry(array, osmKey, osmValue, tags, onlyKey, arrayName) {
	for (let i in array) {
		const x = array[i];

		const keyMatches   = !x.matchKey   || x.matchKey.test(osmKey);
		if (!keyMatches) {
			continue;
		} else if (onlyKey) {
			return x;
		}

		const valueMatches = !x.matchValue || x.matchValue.test(osmValue);
		if (!valueMatches) {
			console.log(`Key ${osmKey} matched with ${arrayName}[${i}], but value ${osmValue} didn't`);
			continue;
		}

		const conditionMatches = (
			x.condition === undefined || tags === undefined ||
			x.condition(osmKey, osmValue, tags)
		);
		if (!conditionMatches) {
			console.log(`Tag ${osmKey}=${osmValue} matched with ${arrayName}[${i}], but condition function returned false`);
		}

		return x;
	}
	return undefined;
}

function matchingValueSplitter(osmKey, osmValue, tags, onlyKey) {
	return matchingEntry(valueSplitters, osmKey, osmValue, tags, onlyKey, "valueSplitters");
}

function matchingTagFormatter(osmKey, osmValue, tags, onlyKey) {
	return matchingEntry(tagFormatters, osmKey, osmValue, tags, onlyKey, "tagFormatters");
}

waitForKeyElements(() => {
	// Find <tr> elements that have a <th> where the innerText matches any
	// of the keys of tagFormatters (interpreted as RegExpes)
	const rows = document.querySelectorAll("table.browse-tag-list tr");
	return [...rows].filter(el => {
		const [osmKey, osmValue] = keyValueOfTableRow(el);
		return matchingTagFormatter(osmKey, osmValue, undefined, true) !== undefined;
	});
}, el => {

	// Invariable: el will have at least one tagFormatter that matches its key.
	// However, its subvalues (value after splitting) may not match.

	const [osmKey, osmValue] = keyValueOfTableRow(el);

	const tags = tagsOfTbody(el.parentElement);

	const valueSplitter = matchingValueSplitter(osmKey, osmValue, tags);
	const valueSeparator = valueSplitter === undefined || valueSplitter.valueSeparator === undefined
		? defaultValueSeparator : valueSplitter.valueSeparator;

	let osmValueList = valueSeparator === false ? [osmValue] : osmValue.split(valueSeparator);

	let elementsToInsert = osmValueList.map((valueOrSeparator, idx) => {
		// Elements at even indices are (sub)values, at odd indices are separators
		if (idx % 2 === 0) {
			// Value
			const osmSubValue = valueOrSeparator;
			const tagFormatter = matchingTagFormatter(osmKey, osmSubValue, tags, false);

			if (!tagFormatter) {
				return document.createTextNode(osmSubValue);
			}

			const uri = tagFormatter.uri(osmKey, osmSubValue);
			if (!uri) {
				console.log(
					`Not linkifying ${osmKey}=${osmSubValue} because the uri function returned a ` +
					`falsy value (empty string, null, undefined, ...)`
				);
				return document.createTextNode(osmSubValue);
			}

			console.log(`Linkifying ${osmKey}=${osmSubValue} (${uri})`);

			const aEl = document.createElement("a");
			aEl.href = uri;
			aEl.innerText = osmSubValue;
			if (tagFormatter.title) {
				aEl.title = tagFormatter.title(osmKey, osmSubValue);
			}
			return aEl;

		} else {
			// Separator
			return document.createTextNode(valueOrSeparator);
		}
	});

	// If only text nodes are produced, don't bother replacing.
	// (It's even possible that we would be overwriting pre-existing links)
	const anythingChanged = !elementsToInsert.every(el => el.nodeType === document.TEXT_NODE);

	if (anythingChanged) {
		const tableCell = el.querySelector("td");
		tableCell.innerHTML = "";
		tableCell.append(...elementsToInsert);
	}

}, interval * 1000);
